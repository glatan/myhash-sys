use std::fs;

fn get_paths(dir: &str) -> Vec<String> {
    fs::read_dir(dir)
        .unwrap()
        .filter(|entry| entry.as_ref().unwrap().file_type().unwrap().is_file())
        .map(|file| file.unwrap().path().to_str().unwrap().to_owned())
        .collect()
}

fn main() {
    let project_dir = env!("CARGO_MANIFEST_DIR");

    let third_party_files = get_paths(&format!("{}/src/c/third_party/", project_dir));
    cc::Build::new()
        .warnings(true)
        .flag("-Wall")
        .flag("-Wextra")
        .files(third_party_files)
        .compile("libthirdparty.a");

    let c_files = get_paths(&format!("{}/src/c/", project_dir));
    cc::Build::new()
        .warnings(true)
        .flag("-Wall")
        .flag("-Wextra")
        .files(c_files)
        .compile("libmyhashc.a");

    let cpp_files = get_paths(&format!("{}/src/cpp/", project_dir));
    cc::Build::new()
        .cpp(true)
        .compiler("g++")
        .warnings(true)
        .flag("-Wall")
        .flag("-Wextra")
        .files(cpp_files)
        .compile("libmyhashcpp.a");

    println!("cargo:rustc-link-lib=crypto");
    println!("cargo:rustc-link-lib=cryptopp");
    println!("cargo:rustc-link-lib=XKCP");
}
