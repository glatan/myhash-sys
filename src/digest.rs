pub trait Digest<T = Self> {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8>;
    fn hash_to_lowerhex(&mut self, message: &[u8]) -> String {
        self.hash_to_bytes(message)
            .iter()
            .map(|byte| format!("{:02x}", byte))
            .collect()
    }
    fn hash_to_upperhex(&mut self, message: &[u8]) -> String {
        self.hash_to_bytes(message)
            .iter()
            .map(|byte| format!("{:02X}", byte))
            .collect()
    }
}
