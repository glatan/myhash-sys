#include <stdlib.h>

#include "third_party/include/global.h"
#include "third_party/include/md2.h"

void md2(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    MD2_CTX ctx;
    MD2Init(&ctx);
    MD2Update(&ctx, message, message_len);
    MD2Final(digest, &ctx);
}
