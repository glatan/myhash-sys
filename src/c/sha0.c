#include <stdlib.h>

#include "third_party/include/sha0.h"

void sha0(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    SHA0Context ctx;
    SHA0Reset(&ctx);
    SHA0Input(&ctx, message, message_len);
    SHA0Result(&ctx, digest);
}
