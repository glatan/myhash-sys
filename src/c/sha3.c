#include <xkcp/KeccakHash.h>
#include <xkcp/KeccakSponge.h>

void sha3_224(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    Keccak_HashInstance ctx;
    Keccak_HashInitialize(&ctx, 1152, 1600 - 1152, 224, 0x06);
    Keccak_HashUpdate(&ctx, message, message_len);
    Keccak_HashFinal(&ctx, digest);
}

void sha3_256(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    Keccak_HashInstance ctx;
    Keccak_HashInitialize(&ctx, 1088, 1600 - 1088, 256, 0x06);
    Keccak_HashUpdate(&ctx, message, message_len);
    Keccak_HashFinal(&ctx, digest);
}

void sha3_384(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    Keccak_HashInstance ctx;
    Keccak_HashInitialize(&ctx, 832, 1600 - 832, 384, 0x06);
    Keccak_HashUpdate(&ctx, message, message_len);
    Keccak_HashFinal(&ctx, digest);
}

void sha3_512(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    Keccak_HashInstance ctx;
    Keccak_HashInitialize(&ctx, 576, 1600 - 576, 512, 0x06);
    Keccak_HashUpdate(&ctx, message, message_len);
    Keccak_HashFinal(&ctx, digest);
}

void shake128(const BitSequence *message, size_t message_len, uint8_t *digest, size_t digest_len)
{
    Keccak_HashInstance ctx;
    Keccak_HashInitialize(&ctx, 1344, 1600 - 1344, digest_len * 8, 0x1F);
    Keccak_HashUpdate(&ctx, message, message_len);
    Keccak_HashFinal(&ctx, digest);
}

void shake256(const BitSequence *message, size_t message_len, uint8_t *digest, size_t digest_len)
{
    Keccak_HashInstance ctx;
    Keccak_HashInitialize(&ctx, 1088, 1600 - 1088, digest_len * 8, 0x1F);
    Keccak_HashUpdate(&ctx, message, message_len);
    Keccak_HashFinal(&ctx, digest);
}
