#include <stdlib.h>

#include "third_party/include/global.h"
#include "third_party/include/md4.h"

void md4(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    MD4_CTX ctx;
    MD4Init(&ctx);
    MD4Update(&ctx, message, message_len);
    MD4Final(digest, &ctx);
}
