#include <stdlib.h>

#include "third_party/include/global.h"
#include "third_party/include/md5.h"

void md5(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    MD5_CTX ctx;
    MD5Init(&ctx);
    MD5Update(&ctx, message, message_len);
    MD5Final(digest, &ctx);
}
