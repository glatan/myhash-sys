#include <stdint.h>
#include <stdlib.h>

#include "third_party/include/blake_ref_round2.h"

void blake28(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    BLAKE_Round2_Hash(224, message, message_len, digest);
}

void blake32(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    BLAKE_Round2_Hash(256, message, message_len, digest);
}

void blake48(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    BLAKE_Round2_Hash(384, message, message_len, digest);
}

void blake64(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    BLAKE_Round2_Hash(512, message, message_len, digest);
}
