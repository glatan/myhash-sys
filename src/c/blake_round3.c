#include <stdint.h>
#include <stdlib.h>

#include "third_party/include/blake_ref_round3.h"

void blake224(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    BLAKE_Round3_Hash(224, message, message_len, digest);
}

void blake256(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    BLAKE_Round3_Hash(256, message, message_len, digest);
}

void blake384(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    BLAKE_Round3_Hash(384, message, message_len, digest);
}

void blake512(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    BLAKE_Round3_Hash(512, message, message_len, digest);
}
