#include <stdlib.h>

#include "third_party/include/sha1.h"

void sha1(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    SHA1Context ctx;
    SHA1Reset(&ctx);
    SHA1Input(&ctx, message, message_len);
    SHA1Result(&ctx, digest);
}
