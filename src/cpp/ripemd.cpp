#include <cryptopp/ripemd.h>

using namespace CryptoPP;

extern "C" void ripemd128(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    RIPEMD128 hash;
    hash.Update(message, message_len);
    hash.Final(digest);
}

extern "C" void ripemd160(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    RIPEMD160 hash;
    hash.Update(message, message_len);
    hash.Final(digest);
}

extern "C" void ripemd256(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    RIPEMD256 hash;
    hash.Update(message, message_len);
    hash.Final(digest);
}

extern "C" void ripemd320(const uint8_t *message, size_t message_len, uint8_t *digest)
{
    RIPEMD320 hash;
    hash.Update(message, message_len);
    hash.Final(digest);
}
