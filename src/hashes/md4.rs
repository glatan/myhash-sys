use crate::digest::Digest;

pub struct Md4;

extern "C" {
    fn md4(message: *const u8, message_len: usize, digest: &mut [u8; 16]);
}

impl Md4 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Md4 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 16];

        unsafe {
            md4(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Md4};

    const TEST_VECTORS: [(&[u8], &str); 7] = [
        // https://tools.ietf.org/html/rfc1320
        ("".as_bytes(), "31d6cfe0d16ae931b73c59d7e0c089c0"),
        ("a".as_bytes(), "bde52cb31de33e46245e05fbdbd6fb24"),
        ("abc".as_bytes(), "a448017aaf21d8525fc10ae87aa6729d"),
        (
            "message digest".as_bytes(),
            "d9130a8164549fe818874806e1c7014b",
        ),
        (
            "abcdefghijklmnopqrstuvwxyz".as_bytes(),
            "d79e1c308aa5bbcdeea8ed63df412da9",
        ),
        (
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".as_bytes(),
            "043f8582f241db351ce627e153e7f0e4",
        ),
        (
            "12345678901234567890123456789012345678901234567890123456789012345678901234567890"
                .as_bytes(),
            "e33b4ddc9c38f2199c3e7b164fcc0536",
        ),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Md4::new().hash_to_lowerhex(m), *e);
        }
    }
}
