use crate::digest::Digest;

pub struct Sha1;

extern "C" {
    fn sha1(message: *const u8, message_len: usize, digest: &mut [u8; 20]);
}

impl Sha1 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Sha1 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 20];

        unsafe {
            sha1(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Sha1};

    const TEST_VECTORS: [(&[u8], &str); 4] = [
        // https://tools.ietf.org/html/rfc3174
        ("abc".as_bytes(), "a9993e364706816aba3e25717850c26c9cd0d89d"),
        (
            "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".as_bytes(),
            "84983e441c3bd26ebaae4aa1f95129e5e54670f1",
        ),
        ("a".as_bytes(), "86f7e437faa5a7fce15d1ddcb9eaeaea377667b8"),
        (
            "0123456701234567012345670123456701234567012345670123456701234567".as_bytes(),
            "e0c094e867ef46c350ef54a7f59dd60bed92ae83",
        ),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Sha1::new().hash_to_lowerhex(m), *e);
        }
    }
}
