use crate::digest::Digest;

extern "C" {
    fn blake32(message: *const u8, message_len: usize, digest: &mut [u8; 32]);
}

pub struct Blake32;

impl Blake32 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Blake32 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 32];

        unsafe {
            blake32(message.as_ptr(), message.len() * 8, &mut digest);
        }

        digest.to_vec()
    }
}
