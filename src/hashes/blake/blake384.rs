use crate::digest::Digest;

extern "C" {
    fn blake384(message: *const u8, message_len: usize, digest: &mut [u8; 48]);
}

pub struct Blake384;

impl Blake384 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Blake384 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 48];

        unsafe {
            blake384(message.as_ptr(), message.len() * 8, &mut digest);
        }

        digest.to_vec()
    }
}
