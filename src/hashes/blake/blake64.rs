use crate::digest::Digest;

extern "C" {
    fn blake64(message: *const u8, message_len: usize, digest: &mut [u8; 64]);
}

pub struct Blake64;

impl Blake64 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Blake64 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 64];

        unsafe {
            blake64(message.as_ptr(), message.len() * 8, &mut digest);
        }

        digest.to_vec()
    }
}
