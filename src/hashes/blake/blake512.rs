use crate::digest::Digest;

extern "C" {
    fn blake512(message: *const u8, message_len: usize, digest: &mut [u8; 64]);
}

pub struct Blake512;

impl Blake512 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Blake512 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 64];

        unsafe {
            blake512(message.as_ptr(), message.len() * 8, &mut digest);
        }

        digest.to_vec()
    }
}
