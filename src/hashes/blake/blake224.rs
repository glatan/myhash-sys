use crate::digest::Digest;

extern "C" {
    fn blake224(message: *const u8, message_len: usize, digest: &mut [u8; 28]);
}

pub struct Blake224;

impl Blake224 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Blake224 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 28];

        unsafe {
            blake224(message.as_ptr(), message.len() * 8, &mut digest);
        }

        digest.to_vec()
    }
}
