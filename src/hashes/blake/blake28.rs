use crate::digest::Digest;

extern "C" {
    fn blake28(message: *const u8, message_len: usize, digest: &mut [u8; 28]);
}

pub struct Blake28;

impl Blake28 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Blake28 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 28];

        unsafe {
            blake28(message.as_ptr(), message.len() * 8, &mut digest);
        }

        digest.to_vec()
    }
}
