use crate::digest::Digest;

extern "C" {
    fn blake256(message: *const u8, message_len: usize, digest: &mut [u8; 32]);
}

pub struct Blake256;

impl Blake256 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Blake256 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 32];

        unsafe {
            blake256(message.as_ptr(), message.len() * 8, &mut digest);
        }

        digest.to_vec()
    }
}
