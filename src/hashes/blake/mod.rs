// Round1/2 submission version
mod blake28;
mod blake32;
mod blake48;
mod blake64;
pub use blake28::Blake28;
pub use blake32::Blake32;
pub use blake48::Blake48;
pub use blake64::Blake64;

// Final submission version
mod blake224;
mod blake256;
mod blake384;
mod blake512;
pub use blake224::Blake224;
pub use blake256::Blake256;
pub use blake384::Blake384;
pub use blake512::Blake512;
