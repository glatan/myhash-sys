use crate::digest::Digest;

pub struct Md5;

extern "C" {
    fn md5(message: *const u8, message_len: usize, digest: &mut [u8; 16]);
}

impl Md5 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Md5 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 16];

        unsafe {
            md5(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Md5};

    const TEST_VECTORS: [(&[u8], &str); 7] = [
        // https://tools.ietf.org/html/rfc1321
        ("".as_bytes(), "d41d8cd98f00b204e9800998ecf8427e"),
        ("a".as_bytes(), "0cc175b9c0f1b6a831c399e269772661"),
        ("abc".as_bytes(), "900150983cd24fb0d6963f7d28e17f72"),
        (
            "message digest".as_bytes(),
            "f96b697d7cb7938d525a2f31aaf161d0",
        ),
        (
            "abcdefghijklmnopqrstuvwxyz".as_bytes(),
            "c3fcd3d76192e4007dfb496cca67e13b",
        ),
        (
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".as_bytes(),
            "d174ab98d277d9f5a5611c2c9f419d9f",
        ),
        (
            "12345678901234567890123456789012345678901234567890123456789012345678901234567890"
                .as_bytes(),
            "57edf4a22be3c955ac49da2e2107b67a",
        ),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Md5::new().hash_to_lowerhex(m), *e);
        }
    }
}
