use crate::digest::Digest;

pub struct Ripemd128;

#[link(name = "cryptopp", kind = "dylib")]
extern "C" {
    fn ripemd128(message: *const u8, message_len: usize, digest: &mut [u8; 16]);
}

impl Ripemd128 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Ripemd128 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 16];

        unsafe {
            ripemd128(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Ripemd128};

    const TEST_VECTORS: [(&[u8], &str); 9] = [
        // https://homes.esat.kuleuven.be/~bosselae/ripemd160/pdf/AB-9601/AB-9601.pdf
        ("".as_bytes(), "cdf26213a150dc3ecb610f18f6b38b46"),
        ("a".as_bytes(), "86be7afa339d0fc7cfc785e72f578d33"),
        ("abc".as_bytes(), "c14a12199c66e4ba84636b0f69144c77"),
        (
            "message digest".as_bytes(),
            "9e327b3d6e523062afc1132d7df9d1b8",
        ),
        (
            "abcdefghijklmnopqrstuvwxyz".as_bytes(),
            "fd2aa607f71dc8f510714922b371834e",
        ),
        (
            "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".as_bytes(),
            "a1aa0689d0fafa2ddc22e88b49133a06",
        ),
        (
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".as_bytes(),
            "d1e959eb179c911faea4624c60c5c702",
        ),
        (
            "12345678901234567890123456789012345678901234567890123456789012345678901234567890"
                .as_bytes(),
            "3f45ef194732c2dbb2c4a2c769795fa3",
        ),
        (&[0x61; 1000000], "4a7f5723f954eba1216c9d8f6320431f"),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Ripemd128::new().hash_to_lowerhex(m), *e);
        }
    }
}
