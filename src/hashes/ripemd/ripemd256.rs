use crate::digest::Digest;

pub struct Ripemd256;

#[link(name = "cryptopp", kind = "dylib")]
extern "C" {
    fn ripemd256(message: *const u8, message_len: usize, digest: &mut [u8; 32]);
}

impl Ripemd256 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Ripemd256 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 32];

        unsafe {
            ripemd256(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Ripemd256};

    const TEST_VECTORS: [(&[u8], &str); 9] = [
        // https://homes.esat.kuleuven.be/~bosselae/ripemd160.html
        (
            "".as_bytes(),
            "02ba4c4e5f8ecd1877fc52d64d30e37a2d9774fb1e5d026380ae0168e3c5522d",
        ),
        (
            "a".as_bytes(),
            "f9333e45d857f5d90a91bab70a1eba0cfb1be4b0783c9acfcd883a9134692925",
        ),
        (
            "abc".as_bytes(),
            "afbd6e228b9d8cbbcef5ca2d03e6dba10ac0bc7dcbe4680e1e42d2e975459b65",
        ),
        (
            "message digest".as_bytes(),
            "87e971759a1ce47a514d5c914c392c9018c7c46bc14465554afcdf54a5070c0e",
        ),
        (
            "abcdefghijklmnopqrstuvwxyz".as_bytes(),
            "649d3034751ea216776bf9a18acc81bc7896118a5197968782dd1fd97d8d5133",
        ),
        (
            "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".as_bytes(),
            "3843045583aac6c8c8d9128573e7a9809afb2a0f34ccc36ea9e72f16f6368e3f",
        ),
        (
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".as_bytes(),
            "5740a408ac16b720b84424ae931cbb1fe363d1d0bf4017f1a89f7ea6de77a0b8",
        ),
        (
            "12345678901234567890123456789012345678901234567890123456789012345678901234567890"
                .as_bytes(),
            "06fdcc7a409548aaf91368c06a6275b553e3f099bf0ea4edfd6778df89a890dd",
        ),
        (
            &[0x61; 1000000],
            "ac953744e10e31514c150d4d8d7b677342e33399788296e43ae4850ce4f97978",
        ),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Ripemd256::new().hash_to_lowerhex(m), *e);
        }
    }
}
