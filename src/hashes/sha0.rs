use crate::digest::Digest;

pub struct Sha0;

extern "C" {
    fn sha0(message: *const u8, message_len: usize, digest: &mut [u8; 20]);
}

impl Sha0 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Sha0 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 20];

        unsafe {
            sha0(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Sha0};

    const TEST_VECTORS: [(&[u8], &str); 2] = [
        // https://web.archive.org/web/20180905102133/https://www-ljk.imag.fr/membres/Pierre.Karpman/fips180.pdf
        // https://crypto.stackexchange.com/questions/62055/where-can-i-find-a-description-of-the-sha-0-hash-algorithm/62071#62071
        ("abc".as_bytes(), "0164b8a914cd2a5e74c4f7ff082c4d97f1edf880"),
        (
            "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".as_bytes(),
            "d2516ee1acfa5baf33dfc1c471e438449ef134c8",
        ),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Sha0::new().hash_to_lowerhex(m), *e);
        }
    }
}
