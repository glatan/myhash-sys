use crate::digest::Digest;

#[link(name = "XKCP", kind = "dylib")]
extern "C" {
    fn sha3_384(message: *const u8, message_len: usize, digest: &mut [u8; 48]);
}

pub struct Sha3_384;

impl Sha3_384 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Sha3_384 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 48];

        unsafe {
            sha3_384(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Sha3_384};

    const TEST_VECTORS: [(&[u8], &str); 1] = [
        // https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/SHA3-384_Msg0.pdf
        (
            "".as_bytes(),
            "0c63a75b845e4f7d01107d852e4c2485c51a50aaaa94fc61995e71bbee983a2ac3713831264adb47fb6bd1e058d5f004",
        ),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Sha3_384::new().hash_to_lowerhex(m), *e);
        }
    }
}
