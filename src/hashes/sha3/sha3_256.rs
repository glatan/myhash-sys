use crate::digest::Digest;

#[link(name = "XKCP", kind = "dylib")]
extern "C" {
    fn sha3_256(message: *const u8, message_len: usize, digest: &mut [u8; 32]);
}

pub struct Sha3_256;

impl Sha3_256 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Sha3_256 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 32];

        unsafe {
            sha3_256(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Sha3_256};

    const TEST_VECTORS: [(&[u8], &str); 1] = [
        // https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/SHA3-256_Msg0.pdf
        (
            "".as_bytes(),
            "a7ffc6f8bf1ed76651c14756a061d662f580ff4de43b49fa82d80a4b80f8434a",
        ),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Sha3_256::new().hash_to_lowerhex(m), *e);
        }
    }
}
