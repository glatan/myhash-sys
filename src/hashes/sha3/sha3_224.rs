use crate::digest::Digest;

#[link(name = "XKCP", kind = "dylib")]
extern "C" {
    fn sha3_224(message: *const u8, message_len: usize, digest: &mut [u8; 28]);
}

pub struct Sha3_224;

impl Sha3_224 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Sha3_224 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 28];

        unsafe {
            sha3_224(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Sha3_224};

    const TEST_VECTORS: [(&[u8], &str); 1] = [
        // https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/SHA3-224_Msg0.pdf
        // https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/SHA3-224_1600.pdf
        (
            "".as_bytes(),
            "6b4e03423667dbb73b6e15454f0eb1abd4597f9a1b078e3f5b5a6bc7",
        ),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Sha3_224::new().hash_to_lowerhex(m), *e);
        }
    }
}
