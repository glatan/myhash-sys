use crate::digest::Digest;

#[link(name = "XKCP", kind = "dylib")]
extern "C" {
    fn sha3_512(message: *const u8, message_len: usize, digest: &mut [u8; 64]);
}

pub struct Sha3_512;

impl Sha3_512 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Sha3_512 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 64];

        unsafe {
            sha3_512(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Sha3_512};

    const TEST_VECTORS: [(&[u8], &str); 1] = [
        // https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/SHA3-512_Msg0.pdf
        (
            "".as_bytes(),
            "a69f73cca23a9ac5c8b567dc185a756e97c982164fe25859e0d1dcc1475c80a615b2123af1f5f94c11e3e9402c3ac558f500199d95b6d3e301758586281dcd26",
        ),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Sha3_512::new().hash_to_lowerhex(m), *e);
        }
    }
}
