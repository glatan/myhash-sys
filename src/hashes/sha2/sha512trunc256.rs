use crate::digest::Digest;

#[link(name = "crypto", kind = "dylib")]
extern "C" {
    fn sha512_256(message: *const u8, message_len: usize, digest: &mut [u8; 32]);
}
pub struct Sha512Trunc256;

impl Sha512Trunc256 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Sha512Trunc256 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 32];

        unsafe {
            sha512_256(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Sha512Trunc256};

    const TEST_VECTORS: [(&[u8], &str); 2] = [
        // https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/SHA512_256.pdf
        (
            "abc".as_bytes(),
            "53048e2681941ef99b2e29b76b4c7dabe4c2d0c634fc6d46e0e2f13107e7af23",
        ),
        (
            "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu".as_bytes(),
            "3928e184fb8690f840da3988121d31be65cb9d3ef83ee6146feac861e19b563a",
        ),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Sha512Trunc256::new().hash_to_lowerhex(m), *e);
        }
    }
}
