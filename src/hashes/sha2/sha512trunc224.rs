use crate::digest::Digest;

#[link(name = "crypto", kind = "dylib")]
extern "C" {
    fn sha512_224(message: *const u8, message_len: usize, digest: &mut [u8; 28]);
}

pub struct Sha512Trunc224;

impl Sha512Trunc224 {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Digest for Sha512Trunc224 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        let mut digest = [0u8; 28];

        unsafe {
            sha512_224(message.as_ptr(), message.len(), &mut digest);
        }

        digest.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::{Digest, Sha512Trunc224};

    const TEST_VECTORS: [(&[u8], &str); 2] = [
        // https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/SHA512_224.pdf
        (
            "abc".as_bytes(),
            "4634270f707b6a54daae7530460842e20e37ed265ceee9a43e8924aa",
        ),
        (
            "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu".as_bytes(),
            "23fec5bb94d60b23308192640b0c453335d664734fe40e7268674af9",
        ),
    ];

    #[test]
    fn test() {
        for (m, e) in TEST_VECTORS.iter() {
            assert_eq!(Sha512Trunc224::new().hash_to_lowerhex(m), *e);
        }
    }
}
