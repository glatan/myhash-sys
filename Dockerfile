FROM registry.gitlab.com/glatan/dockerfiles:mydev

WORKDIR /workdir

RUN \
    pacman -Syyu --noconfirm && \
    pacman -S crypto++ xkcp-git rustup --noconfirm && \
    rustup default stable
