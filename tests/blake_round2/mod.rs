use crate::impl_short_msg_kat;

impl_short_msg_kat!(Blake28, Blake28::new(), 28, "ShortMsgKAT_224.txt");
impl_short_msg_kat!(Blake32, Blake32::new(), 32, "ShortMsgKAT_256.txt");
impl_short_msg_kat!(Blake48, Blake48::new(), 48, "ShortMsgKAT_384.txt");
impl_short_msg_kat!(Blake64, Blake64::new(), 64, "ShortMsgKAT_512.txt");
