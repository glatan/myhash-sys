use crate::impl_short_msg_kat;

impl_short_msg_kat!(Blake224, Blake224::new(), 28, "ShortMsgKAT_224.txt");
impl_short_msg_kat!(Blake256, Blake256::new(), 32, "ShortMsgKAT_256.txt");
impl_short_msg_kat!(Blake384, Blake384::new(), 48, "ShortMsgKAT_384.txt");
impl_short_msg_kat!(Blake512, Blake512::new(), 64, "ShortMsgKAT_512.txt");
