mod blake_round2;
mod blake_round3;

#[macro_export]
macro_rules! impl_short_msg_kat {
    ($T:ident, $Hasher:expr, $DIGEST_BYTE_LEN:expr, $FILE_NAME:expr) => {
        #[allow(non_snake_case)]
        mod $T {
            use myhash_sys::{$T, Digest};
            use std::{
                fs::File,
                io,
                io::{BufRead, BufReader},
            };

            fn hex_to_bytes(s: &str) -> Vec<u8> {
                // 上位4ビット
                let s1: Vec<u8> = s
                    .chars()
                    .by_ref()
                    .enumerate()
                    .filter(|(i, _)| i % 2 == 0)
                    .map(|(_, c)| (c.to_digit(16).unwrap() as u8) << 4)
                    .collect();
                // 下位4ビット
                let s2: Vec<u8> = s
                    .chars()
                    .by_ref()
                    .enumerate()
                    .filter(|(i, _)| i % 2 == 1)
                    .map(|(_, c)| c.to_digit(16).unwrap() as u8)
                    .collect();
                if s1.len() != s2.len() {
                    unreachable!();
                }
                let bytes = {
                    let mut bytes: Vec<u8> = Vec::new();
                    for i in 0..s1.len() {
                        bytes.push((s1[i] & 0b1111_0000) | (s2[i] & 0b0000_1111));
                    }
                    bytes
                };
                bytes
            }

            #[test]
            #[allow(unused_assignments)]
            fn short_msg_kat() -> io::Result<()> {
                if (!$FILE_NAME.is_empty()) {
                    let path = format!("{}{}", file!().strip_suffix("mod.rs").unwrap(), $FILE_NAME);
                    let file = File::open(path)?;

                    let mut message_len = 0usize;
                    let mut message = Vec::with_capacity(2032);
                    let mut digest = String::with_capacity($DIGEST_BYTE_LEN);

                    for line in BufReader::new(file).lines() {
                        if let Ok(line) = line {
                            if (line.starts_with("Len")) {
                                message_len = line
                                    .trim_start_matches("Len = ")
                                    .to_string()
                                    .parse()
                                    .unwrap();
                            }
                            // only test multiple bytes of message
                            if message_len % 8 == 0 {
                                if (line.starts_with("Msg")) {
                                    let message_string =
                                        line.trim_start_matches("Msg = ").to_string();
                                    message = hex_to_bytes(&message_string);
                                }
                                if (line.starts_with("MD")) {
                                    digest = line.trim_start_matches("MD = ").to_string();
                                    assert_eq!(
                                        $Hasher
                                            .hash_to_upperhex(message[0..message_len / 8].as_ref()),
                                        digest
                                    );
                                }
                            }
                        }
                    }
                }

                Ok(())
            }
        }
    };
}
