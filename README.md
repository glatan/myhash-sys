# MyHash-sys

## Dependencies

### MD2

md2.c, md2.h, global.h from [RFC1319](https://tools.ietf.org/html/rfc1319)

### MD4

md4.c, md4.h from [RFC1320](https://tools.ietf.org/html/rfc1320)

### MD5

md5.c, md5.h from [RFC1321](https://tools.ietf.org/html/rfc1321)

## RIPEMD-{128, 160, 256, 320}

Depends on Crypto++(libcryptopp.so, cryptopp/ripemd.h)

### SHA-0

sha1.c, sha1.h from [RFC3174](https://tools.ietf.org/html/rfc3174)

Replace
`W[t] = SHA1CircularShift(1, W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16]);`
to
`W[t] = W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16];`

and rename sha1{.c, .h} to sha0{.c, .h}

### SHA-1

sha1.c, sha1.h from [RFC3174](https://tools.ietf.org/html/rfc3174)

### SHA-2

Depends on OpenSSL(libcrypto.so, evp.h, sha.h)
